'''
File: fold.py
Project: model
File Created: Wednesday, 14th February 2018 5:44:43 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Wednesday, 9th May 2018 1:55:14 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

from math import atan2

class Fold:
    def __init__(self, point, angle, dx):
        self.point = point
        self.angle = angle
        self.dx = dx
    
    def __str__(self):
        return "Fold(" + str(self.point) + ", " + str(self.angle) + ", " + str(self.dx) + ", " + ")"

    def __eq__(self, other):
        return self.point == other.point and self.angle == other.angle and self.dx == other.dx 

from helpers import get_robot_controller

print("beginning script")

# some joint configurations:
config_start = [20.76, 33.05, 2.4, 1.32, 85.11, -82.38]
config_first_connection = [10.05, 61.67, -31.03, 0.06,
                           42.62, -86.45]  # first two magnets attached to shirt
# before flipping stick for third connection
config_before_flip = [10.05, 61.67, -31.02, 0.06, 30.04, -86.45]
config_before_flip_2 = [22.44, 58.36, -27.11, 14.42,
                        41.49, -95.87]  # move over so the shirt doesnt drag            
# all magnets connected to shirt
config_begin_second_connection = [10.05, 61.68, -31.02, 4.41, 30.04, -278.18]
# after small sweep to make sure of connection
config_end_second_connection = [10.05, 61.69, -31.02, -9.34, 30.04, -272.6]
config_lift = [10.05, 22.63, 8.58, -9.34, 98.49, -271.46]
# the robot twists the stick to move the shirt down itself (instead of shaking)
config_twist = [10.05, 22.63, 8.58, -9.34, 98.49, -88.76]
config_up = [10.05, 42.34, -78.81, -1.21, 96.88, -127.56]

config_over_edge = [20.18, 79.2, -89.73, -5.38, 117.19, -80.64]
config_laid_out = [10.05, 40.24, 10.72, 0.57, 53.95, -86.63]
config_pointed_out = [16.8, 59.01, -89.7, -5.38, 117.19, -162.36]

config_shake = [10.05, 42.35, -103.72, -1.21, 117.15, -79.95]
config_up_level = [10.05, 42.34, -78.81, -1.21, 96.88, -84.37]
config_scrape_a = [-13.44, 35.6, -5.92, -1.21, 96.88, -84.37]
config_scrape_b = [56.72, 35.6, -5.92, -1.21, 96.88, -84.37]

config_before_drag = [10.05, 77.81, -78.81, -1.21, 96.88, -90.95]
config_mid_drag = [10.05, 48.98, -19.25, -1.21, 96.88, -90.95]
config_after_drag = [13.97, 29.45, 40.45, 5.86, 56.08, -90.42]

# R.set_joints(config_start)
# raise ValueError("sdfgsdf")

(robot, succeeded) = get_robot_controller()
if succeeded:
    robot.set_joints(config_start)
    robot.set_joints(config_first_connection)
    robot.set_joints(config_before_flip)
    robot.set_joints(config_before_flip_2)
    robot.set_joints(config_begin_second_connection)
    robot.set_joints(config_end_second_connection)
    robot.set_joints(config_lift)
    robot.set_joints(config_twist)

    robot.set_joints(config_scrape_a)
    robot.set_joints(config_scrape_b)

    # robot.set_joints(config_before_drag)
    # robot.set_joints(config_mid_drag)
    # robot.set_joints(config_after_drag)

    print(robot.get_joints())

print("Finished executing script.")


# Deprecated::
# config_next = [17.61, 58.84, -52.85, -0.27, 100.96, 99.51]
# config_lying_outstretched = [12.16, 86.85, -89.34, -0.28, 100.96, 97.09]
# config_lying_second = [12.39, 62.07, -44.0, -0.28, 100.95, 97.09] # should have attached to shirt by now

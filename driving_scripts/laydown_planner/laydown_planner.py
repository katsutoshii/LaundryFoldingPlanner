# NOTE: requires __package__ to be None 
# (see StackExchange: "Attempted relative import in non package")
from os import path
import sys
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from constants import RULER_LENGTH
from numpy import array, array_equal, append
from math import sin, cos, pi
from helpers import within_range, direction
from laydown_config import LaydownConfiguration


def plan(fold_instructions):
    assert fold_instructions.__class__.__name__ == 'FoldInstructions'
    angle = fold_instructions.fold_angle

    # y-distance between the effector and the gap:
    ydist = sin(angle) * fold_instructions.intercept

    # determine four configs (see notebook for details)
    ruler_a = angle
    ruler_b = 2 * pi - angle
    ruler_c = pi- angle
    ruler_d = pi + angle

    c_a = LaydownConfiguration(-ydist, ruler_a, ruler_a + pi/2)
    c_b = LaydownConfiguration(ydist, ruler_b, ruler_b - pi/2)
    c_c = LaydownConfiguration(-ydist, ruler_c, ruler_c - pi/2)
    c_d = LaydownConfiguration(ydist, ruler_d, ruler_d + pi/2)

    return [c_a, c_b, c_c, c_d]

def choose_best(paths):
    """ Test each of the 4 paths with different y-translations;
        returns the best one, and assigns it the appropriate y-values.
        
         """
    for path in paths:

        # TODO iterate over various Y-translations

        # TODO check for out of reach
        # TODO check for collisions w. self (arm)
        # TODO compare distances of workable laydown paths
        print("tested path")

    print("function incomplete")

def within_reach(laydown_path):
    print("not implemented")

def check_collisions(laydown_path):
    print("not implemented")

def assign_value(laydown_path):
    """ Given a laydown option, which we assume is valid, 
        assign a value to represent desirability (e.g., nearness
        to the base of the arm). Higher value -> better path. """
    print("not implemented")

# NOTE: requires __package__ to be None 
# (see StackExchange: "Attempted relative import in non package")
from os import path
import sys
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from laydown_planner import plan, choose_best
from numpy import array
from math import pi, sin, cos
from helpers import within_range, direction, fix_angle, rotate
from laydown_path import LaydownPath
from fold_instructions import FoldInstructions
from constants import LEFT

# TODO: continue testing here
instr = FoldInstructions(250, pi/6)
options = plan(instr)   
paths = []
for option in options:
    paths += [LaydownPath(option, 250, 250)]
choose_best(paths)




# import Tkinter as tk
# import math, cmath

# window = tk.Tk()
# window.geometry('500x500')
# canvas = tk.Canvas(window, width=500, height=500)
# canvas.pack()
# center = (250, 250)
# coords = [(250, 248),(400, 248), (400, 252), (250, 252)]
# polygon = canvas.create_polygon(coords)
# angle = cmath.exp(pi * 0.25 * 1j)
# offset = complex(center[0], center[1])
# newxy = []
# for x, y in coords:
#     v = angle * (complex(x, y) - offset) + offset
#     newxy.append(v.real)
#     newxy.append(v.imag)
# canvas.coords(polygon, *newxy)
# window.mainloop()

print("testing script concluded")
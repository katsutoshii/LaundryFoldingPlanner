'''
File: path_processor.py
Project: driving_scripts
File Created: Friday, 20th April 2018 2:43:50 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Friday, 20th April 2018 2:43:53 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

from math import atan2, pi
from paths import T_SHIRT_PATH

def process_path(path):
    angles = []

    prev_angle = 0  # in radians

    # for each fold, calculate the angle between the lines
    for fold in path:
        # calculate the abolsute angle for comparison to previous configuration
        relative_angle = fold.angle() - prev_angle
        angles.append(relative_angle * 180 / pi)

        # udpate the previous angle
        prev_angle = relative_angle

    return angles

if __name__ == "__main__":
    print process_path(T_SHIRT_PATH)

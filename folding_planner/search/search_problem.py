'''
File: search_problem.py
Project: search
File Created: Tuesday, 23rd January 2018 5:41:02 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Wednesday, 24th January 2018 4:25:15 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

from abc import abstractmethod, ABCMeta

class SearchProblem():
    __metaclass__ = ABCMeta

    def __init__(self):
        pass
    
    @abstractmethod
    def get_start_state(self):
        """Method to get a starting state for the problem
        
        Decorators:
            abstractmethod
        """

        pass

    @abstractmethod
    def get_successors(self, state):
        """Method to get the successors from a given state
        
        Decorators:
            abstractmethod
        
        Arguments:
            state {state} -- The current state
        """

        pass

    @abstractmethod
    def is_goal(self, state):
        """Method to check if a state is a goal
        
        Decorators:
            abstractmethod
        
        Arguments:
            state {state} -- The current state
        """

        pass
    
    @abstractmethod
    def get_transition_cost(self, state1, state2):
        """Method to get the transition cost between two states
        
        Decorators:
            abstractmethod
        
        Arguments:
            state1 {state} -- Start state
            state2 {state} -- End state
        """

        pass

    @abstractmethod
    def problem_is_valid(self):
        """Method to check if a problem is valid
        
        Decorators:
            abstractmethod
        """

        pass
    
    @abstractmethod
    def draw_state(self, state):
        """Method to draw a state
        
        Decorators:
            abstractmethod
        
        Arguments:
            state {state} -- the state to draw
        """
        pass

'''
File: search_solution.py
Project: search
File Created: Tuesday, 23rd January 2018 5:40:52 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 23rd January 2018 5:41:47 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

class SearchSolution:
    def __init__(self, problem, search_method):
        self.problem_name = str(problem)
        self.search_method = search_method
        self.path = []
        self.nodes_visited = 0
        self.cost = 0

    def __str__(self):
        string = "----\n"
        string += "{:s}\n"
        string += "attempted with search method {:s}\n"

        if self.path:

            string += "number of nodes visited: {:d}\n"
            string += "solution length: {:d}\n"
            string += "cost: {:d}\n"
            string += "path: {:s}\n"

            string = string.format(self.problem_name, self.search_method,
                                   self.nodes_visited, len(self.path), self.cost, str(self.path))
        else:
            string += "no solution found after visiting {:d} nodes\n"
            string = string.format(self.problem_name, self.search_method, self.nodes_visited)

        return string

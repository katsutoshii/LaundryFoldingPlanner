'''
File: greedy_search.py
Project: search
File Created: Tuesday, 27th February 2018 6:17:42 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 27th February 2018 6:19:36 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

from search_solution import SearchSolution
from search import SearchNode, select_max_successor


def greedy_search(search_problem, scoring_fn, search_limit=5000000, debug=False):
    """Method to perform greedy search on a search problem

    Arguments:
        search_problem {SearchProblem} -- the problem to be searched over
        scoring_fn {method} -- function to score a state

    Keyword Arguments:
        search_limit {int} -- max number of visitable nodes (default: {5000000})
        debug {bool} -- whether to show debugging statements or not (default: {False})
    """
    solution = SearchSolution(
        search_problem, "Greedy with scoring function " + scoring_fn.__name__)

    # initial checks to make sure that the problem is solvable
    if not search_problem.problem_is_valid():
        return solution

    start_state = search_problem.get_start_state()
    solution.path.append(start_state)
    node = SearchNode(start_state)

    # continue searching until we break out or run out of nodes
    while solution.nodes_visited < search_limit:

        if debug:
            print "Checking state", node.state
            search_problem.draw_state(node.state)

        # call helper function to get the max scoring
        successors = search_problem.get_successors(node.state)
        node = select_max_successor(search_problem, successors, scoring_fn, set())
        solution.path.append(node.state)
        if search_problem.is_goal(node.state):
            break

    return solution

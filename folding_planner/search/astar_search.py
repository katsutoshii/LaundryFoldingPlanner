'''
File: astar_search.py
Project: folding_planner
File Created: Tuesday, 23rd January 2018 3:21:02 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 23rd January 2018 5:47:25 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

from heapq import heappush, heappop
from search_solution import SearchSolution
from search import SearchNode, backchain


class AstarNode(SearchNode):
    """Class for an AstarNode
    """

    def __init__(self, state, heuristic=0, parent=None, transition_cost=0):
        """Constructor for AstarNode

        Arguments:
            state {[type]} -- [description]
            heuristic {[type]} -- [description]

        Keyword Arguments:
            parent {[type]} -- [description] (default: {None})
            transition_cost {[type]} -- [description] (default: {0})
        """
        super(AstarNode, self).__init__().__init__(state, parent=parent)

        self.heuristic = heuristic
        self.transition_cost = transition_cost

    def priority(self):
        """
        Method to return the priority of this node
        :return priority:
        """
        return self.transition_cost + self.heuristic

    # comparison operator,
    # needed for heappush and heappop to work with AstarNodes:
    def __lt__(self, other):
        return self.priority() < other.priority()


def astar_search(search_problem, heuristic_fn, search_limit=5000000, debug=False):
    """Method to perform astar search on a search problem

    Arguments:
        search_problem {[type]} -- [description]
        heuristic_fn {[type]} -- [description]

    Keyword Arguments:
        search_limit {[type]} -- [description] (default: {5000000})

    Returns:
        [type] -- [description]
    """

    solution = SearchSolution(
        search_problem, "Astar with heuristic " + heuristic_fn.__name__)

    # initial checks to make sure that the problem is solvable
    if not search_problem.problem_is_valid():
        return solution

    # initialize data structures
    start_state = search_problem.get_start_state()
    start_node = AstarNode(start_state, heuristic_fn(start_state))

    pqueue = []
    heappush(pqueue, start_node)
    # dictionary to hold the cost of visited tiles
    visited_cost = {start_node.state: 0}

    # continue the loop until the queue runs out (exhausted all possibilities,
    # never reached the goal)
    while pqueue and solution.nodes_visited < search_limit:

        curr_node = heappop(pqueue)     # examine the next node
        solution.nodes_visited += 1     # count visiting this node

        if debug:
            print "Checking state", curr_node.state
            search_problem.draw_state(curr_node.state)

        if search_problem.is_goal(curr_node.state):     # if the node is the goal
            # get the path from the start to this node
            solution.path = backchain(curr_node)
            solution.cost = curr_node.transition_cost
            return solution

        # otherwise continue by getting the next states (guaranteed safe!)
        for next_state in search_problem.get_successors(curr_node.state):

            # build the next node
            next_node = AstarNode(next_state, heuristic_fn(next_state), parent=curr_node,
                                  transition_cost=visited_cost[curr_node.state] +
                                  search_problem.get_transition_cost(curr_node.state, next_state))

            # if the next state is unvisited or we found a better way to get to it,
            # add them to the queue and update the visited dictionary
            if next_state not in visited_cost or \
                    next_node.transition_cost < visited_cost[next_state]:
                heappush(pqueue, next_node)
                visited_cost[next_state] = next_node.transition_cost
    return solution

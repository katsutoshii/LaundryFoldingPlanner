'''
File: greedy_search.py
Project: search
File Created: Tuesday, 27th February 2018 6:17:42 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Tuesday, 27th February 2018 6:19:36 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

from search_solution import SearchSolution
from search import SearchNode, select_max_successor

def prioritizing_dfs(search_problem, scoring_fn, min_score, search_limit=5000000, debug=False):
    """Method to perform greedy search on a search problem
    
    Arguments:
        search_problem {SearchProblem} -- the problem to be searched over
        scoring_fn {method} -- function to score a state
    
    Keyword Arguments:
        search_limit {int} -- max number of visitable nodes (default: {5000000})
        debug {bool} -- whether to show debugging statements or not (default: {False})
    """
    solution = SearchSolution(search_problem, "Prioritizing DFS with scoring function " + scoring_fn.__name__)

    # initial checks to make sure that the problem is solvable
    if not search_problem.problem_is_valid():
        return solution

    start_state = search_problem.get_start_state()
    solution.path.append(start_state)
    visited = set()

    # continue searching until we break out or run out of nodes
    while solution.nodes_visited < search_limit:
        # if empty, break
        if not solution.path:
            break

        # otherwise examine the current state (last element of list)
        curr_state = solution.path[-1]
        visited.add(curr_state)

        if debug:
            print "Checking state", curr_state
            search_problem.draw_state(curr_state)

        # first check if we are the goal
        if search_problem.is_goal(curr_state):
            break

        # call helper function to get the max scoring
        successors = search_problem.get_successors(curr_state)
        max_successor = select_max_successor(search_problem, successors, scoring_fn, visited)

        # if there are no successors, or we are too bad of a state, backtrack!
        if max_successor is None or scoring_fn(search_problem, curr_state) < min_score:
            solution.path.pop()
            continue

        # otherwise continue
        solution.path.append(max_successor)

    return solution

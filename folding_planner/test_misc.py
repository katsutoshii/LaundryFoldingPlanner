from model.polygon import Polygon

if __name__ == "__main__":
    p = Polygon([
        (1, 1),
        (1, 2),
        (2, 2,), 
        (2, 1),
    ])

    print p.line_intersects((1, 1), (2, 2))

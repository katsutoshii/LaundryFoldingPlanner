'''
File: test_paths.py
Project: folding_planner
File Created: Wednesday, 31st January 2018 4:05:25 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Wednesday, 31st January 2018 4:11:18 pm
Modified By: Josiah Putman (joshikatsu@gmail.com)
'''

from model.fold import Fold

# (100, 130), (150.0, 200.0)
PATH1 = [
]

T_SHIRT_PATH = [
    Fold((150, 200), 0, 0),
    Fold((150, 2), 45, 0),
    Fold((150.0, 72.71067811865474), -45, 0),
]

PATH2 = [
    Fold((150.0, 100.0), 180.0, 0.0, ), 
    Fold((200.0, 50.0), 111.801409486, -50.0, ), 
    Fold((208.30833618759425, 53.85164807134504), 270.0, -58.3083361876, )
]

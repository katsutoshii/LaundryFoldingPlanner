# Folding planner module

This module handles the modelling and search over a state space for the laundry folding problem.

## Model

### `Polygon`

We are modeling a single layer of clothes in a given piece of laundry as a simple polygon. This class uses a lot of the `shapely.geometry.Polygon` functions, but exists as its own class to allow for easier manipulation of the object and drawing inside of Tkinter canvases.

```python
class Polygon:
    def __init__(self, vertices, origin=(0, 0)):
        self.vertices = vertices
        self.origin = origin
        self.item_id = -1

    def insert_point(self, vertex):
        """Method to insert a point into a polygon

        Arguments:
            vertex {tuple} -- point to insert
        """

    def insert_line_intersections(self, point_a, point_b):
        """Method to insert all intersecting points so that the polygon can be properly folded

        Arguments:
            point_a {tuple} -- point A of the line
            point_b {tuple} -- point B of the line
        """
    def draw(self, canvas, outline='black', fill='gray', width=2):
        """Method to draw a polygon

        Arguments:
            canvas {Jkinter.canvas} -- canvas to draw on
        """

    def erase(self, canvas):
        """Method to erase a polygon from the canvas

        Arguments:
            canvas {Pkinter.canvas} -- canvas to be erased from
        """

    def __str__(self):
        return "Polygon {" + str(self.vertices) + "}"

    def __repr__(self):
        return str(self)
```

### `StackedPolygon`

The entire piece of clothing is modelled by the `StackedPolygon` object, which contains a list of `Polygons` layered on top of each other.

```python
class StackedPolygon:
    """A stacked polygon contains one base polygon and any number of stacks, which are other
    stacked polygons
    """

    def __init__(self, polygon=Polygon([]), depth=0):
        """Constructor for a stacked polygon

        Keyword Arguments:
            polygon {Polygon} -- the base of this stacked polygon (default: {None})
        """

    def fold_vertex(self, vertex, new_polygon, new_polygon_folded, point_a, point_b, side, no_flip_points):
        """Method to fold a single vertex about a line, if it is on the right side of the line

        Arguments:
            vertex {tuple} -- vertex to fold
            new_polygon {Polygon} -- polygon to save the vertex in
            point_a {tuple} -- point_a of the fold line
            point_b {tuple} -- point_b of the fold line
            side {boolean} -- which side to fold
        """

    def make_fold(self, point_a, point_b, side, canvas=None):
        """Method to make a fold on a stacked polygon

        Arguments:
            point_a {point} -- point of one end of the fold line
            point_b {point} -- point of the other end of the fold line
            side {bool} -- which side to flip
        """

    def draw(self, canvas, outline='black', fill='gray', width=2):
        """Method to draw a stacked polygon

        Arguments:
            canvas {Jkinter.canvas} -- canvas to draw on
        """

    def draw_fold(self, point_a, point_b, side, canvas):
        """Method to perform and draw a fold

        Arguments:
            point_a {tuple} -- point A of the line to fold across
            point_b {tuple} -- point B of the line to fold across
            side {boolean} -- which side to fold to
            canvas {Pkinter.canvas} -- canvas to draw on
        """

    def redraw(self, canvas, outline='black', fill='gray', width=2):
        """Method to redraw a stacked polygon on a canvas

        Arguments:
            canvas {Pkinter.canvas} -- Canvas to redraw on
        """

    def erase(self, canvas):
        """Method to erase a polygon

        Arguments:
            canvas {Pkinter.canvas} -- canvas to be erased from
        """

    def __str__(self):

    def __repr__(self)
```

## Search

We are currently using A* search to find the best path.

## Visualization

We use `Pkinter` to draw the objects onto a canvas.

## Results

For the T-Shirt:
path: [start, (150.0, 100.0), (150.0, 200.0), True, (150.0, 100.0), (100.0, 150.0), False, (100.0, 150.0), (150.0, 150.0), True]

## Problems

## TODO

* Make the target flip when required
* Make the planner ignore all vertices that pass through the target
* All backtracking for the planner
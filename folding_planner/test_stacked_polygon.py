'''
File: test_stacked_polygon.py
Project: folding_planner
File Created: Wednesday, 24th January 2018 3:35:36 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Wednesday, 24th January 2018 3:35:39 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''
from time import sleep

from model.polygon import Polygon
from model.polygon_graphics import run_polygon_graphics
from model.stacked_polygon import StackedPolygon
from test_shapes import T_SHIRT, TRIANGLE, MEDIUM_SQUARE
from test_paths import PATH1, T_SHIRT_PATH, PATH2

FOLD_LINE_CENTER = 150

def run_fold(fold, stacked_polygon, canvas):

    print "running fold", fold
    print

    # draw a fold
    stacked_polygon.align_and_draw_fold(fold, canvas)
    
    print stacked_polygon
    print

def test_fold_path(path):

    # setup canvas
    target = Polygon(MEDIUM_SQUARE)
    canvas1, canvas2 = run_polygon_graphics("Testing stacked polygon (test1)")

    # stack_textID = canvas1.create_text(
    #     50, 20, text="stack height = 1", anchor='nw')
    # error_textID = canvas1.create_text(
    #     50, 40, text="area diff = 0", anchor='nw')

    base = Polygon(T_SHIRT)
    stacked_polygon = StackedPolygon(base, target=Polygon(MEDIUM_SQUARE), fold_line_center=FOLD_LINE_CENTER)
    stacked_polygon.draw(canvas1)

    # show original shape for a bit

    # run the fold path
    for fold in path:
        run_fold(fold, stacked_polygon, canvas1)

        # canvas1.itemconfigure(stack_textID, text=(
        #     "stack height = " + str(stacked_polygon.height)))
        # canvas1.itemconfigure(error_textID, text=(
        #     "area diff = " + str(stacked_polygon.area_diff())))

    stacked_polygon.draw(canvas1)
    
    # # test silhouette
    # stacked_polygon.erase(canvas1)
    # stacked_polygon.get_silhouette().draw(canvas1)

    # # test the area diff function
    # print stacked_polygon.area_diff(target, canvas=None)
    x = raw_input("done!")


print "running fold path 1"
test_fold_path(T_SHIRT_PATH)

'''
File: test_planner.py
Project: folding_planner
File Created: Monday, 29th January 2018 4:04:01 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Monday, 29th January 2018 4:04:06 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

# from time import sleep
from model.folding_problem import FoldingProblem
from model.polygon import Polygon
from model.polygon_graphics import run_polygon_graphics
from model.stacked_polygon import StackedPolygon
# from search.astar_search import astar_search
# from search.greedy_search import greedy_search
from search.prioritizing_dfs import prioritizing_dfs

# pylint: disable=W0611
from test_shapes import MEDIUM_SQUARE, PANTS, T_SHIRT

FOLDING_LINE_CENTER = 150


def test1():
    canvas1, canvas2 = run_polygon_graphics("Testing planner (test1)")
    start_state = StackedPolygon(
        Polygon(T_SHIRT), fold_line_center=FOLDING_LINE_CENTER,
        target=Polygon(MEDIUM_SQUARE))

    folding_problem = FoldingProblem(start_state, canvas1=canvas1,
                                     canvas2=canvas2, folding_line_center=FOLDING_LINE_CENTER)

    # result = astar_search(folding_problem, FoldingProblem.accuracy_heuristic, debug=True)

    # result = greedy_search(
    #     folding_problem, FoldingProblem.accuracy_heuristic, debug=True)

    result = prioritizing_dfs(
         folding_problem, FoldingProblem.accuracy_heuristic, -100000, debug=True)

    print(result)


test1()

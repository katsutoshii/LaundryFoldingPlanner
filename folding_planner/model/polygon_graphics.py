'''
File: graphics.py
Project: folding_planner
File Created: Wednesday, 24th January 2018 3:08:50 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Wednesday, 24th January 2018 3:22:27 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''

import Tkinter as tk
from threading import Thread
window_width = 800
window_height = 600


def run_polygon_graphics(title):
    # start the main display
    root = tk.Tk()
    root.title(title)

    root.geometry(str(window_width) + "x" + str(window_height))
    # frame = tk.Frame(master=root)
    canvas1 = tk.Canvas(root,
                        width=window_width / 2,
                        height=window_height)

    canvas1.pack(side=tk.LEFT, expand=tk.YES, fill=tk.BOTH)

    canvas2 = tk.Canvas(root,
                        width=window_width / 2,
                        height=window_height)

    canvas2.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.BOTH)
    
    thread = Thread(target=root.mainloop, args=())
    thread.setDaemon = False
    thread.start()
    
    return canvas1, canvas2

'''
File: math_lib.py
Project: folding_planner
File Created: Friday, 26th January 2018 4:22:46 pm
Author: Josiah Putman (joshikatsu@gmail.com)
-----
Last Modified: Friday, 26th January 2018 4:23:57 pm
Modified By: Josiah Putman (joshikatsu@gmail.com>)
'''
from __future__ import division

from shapely.geometry import GeometryCollection
from shapely.ops import polygonize


def dot2D(v1, v2):
    """Calculates the dot product of two 2D vectors
    
    Arguments:
        v1 {tuple} -- vector 1
        v2 {typle} -- vector 2
    
    Returns:
        scalar -- dot product of v1 and v2
    """

    return v1[0] * v2[0] + v1[1] * v2[1]

def cross2D(v1, v2):
    """Calculates the cross product of two 2D vectors
    
    Arguments:
        v1 {tuple} -- vector 1
        v2 {typle} -- vector 2
    
    Returns:
        scalar -- dot product of v1 and v2
    """
    return v1[0] * v2[1] - v1[1] * v2[0]

def add2D(v1, v2):
    """Sums two 2D vectors
    
    Arguments:
        v1 {tuple} -- vector 1
        v2 {typle} -- vector 2
    """

    return (v1[0] + v2[0], v1[1] + v2[1])

def sub2D(v1, v2):
    """Subtracts two 2D vectors
    
    Arguments:
        v1 {tuple} -- vector 1
        v2 {typle} -- vector 2
    """

    return (v1[0] - v2[0], v1[1] - v2[1])

def mult2D(c1, v1):
    """Multiplies a vector by a scalar
    
    Arguments:
        c1 {scalar} -- scalar
        v1 {tuple} -- vector
    """

    return (c1 * v1[0], c1 * v1[1])

def proj2D(v1, v2):
    """Projects one 2D vector onto another
    
    Arguments:
        v1 {tuple} -- vector 1
        v2 {tuple} -- vector 2
    
    Returns:
        scalar -- projection of v1 onto v2
    """
    factor = dot2D(v1, v2)  / (v2[0]**2.0 + v2[1]**2.0)
    return mult2D(factor, v2)


def will_fold(vertex, fold_line_center):
    return vertex[0] < fold_line_center

def split_polygon_line(polygon, line):
    """Method to split a polgon
    Reference:
    https://gist.github.com/snorfalorpagus/46ef62d515e28f0a9e9f

    Arguments:
        polygon {[type]} -- [description]
        line {[type]} -- [description]
    
    Returns:
        [type] -- [description]
    """

    boundary = polygon.boundary
    union = boundary.union(line)
    collection = []

    for geometry in list(polygonize(union)):
        if polygon.contains(geometry.representative_point()):
            collection.append(geometry)

    collection = GeometryCollection(collection)
    return collection

def reflect_point_over_vertical(point, x):
    """Method to mirror a point over a vertical line
    
    Arguments:
        point {tuple} -- point to flip
        x {float} -- line to flip over
    """

    result = (-(point[0] - x) + x, point[1])
    return result


def reflect_point_over_line(point, line_point_a, line_point_b):
    """Method to reflect a point across a line defined by two points
    https://math.stackexchange.com/questions/41298/reflecting-a-point-over-a-line-created-by-two-other-points
    
    Arguments:
        point {tuple} -- the point to reflect
        line_point_a {typle} -- defining point of the line
        line_point_b {tuple} -- defining point of the line

    Returns:
        reflected point -- the point reflected
    """
    P = (point[0] - line_point_a[0], point[1] - line_point_a[1])
    Q = (line_point_b[0] - line_point_a[0], line_point_b[1] - line_point_a[1])

    # k is the intersection between the line AB and line point and point
    K = proj2D(P, Q)

    P1 = sub2D(mult2D(2, K), P)

    return add2D(line_point_a, P1)
    
def dist_sqrd2D(v1, v2):
    """Method to find the distance squared of two 2D vectors
    
    Arguments:
        v1 {tuple} -- vector1
        v2 {tuple} -- vector2
    """

    return (v2[0] - v1[0])**2 + (v2[1] - v1[1])**2

def avg2D(v1, v2):
    """Method to return a vector that is the average of the two input vectors
    
    Arguments:
        v1 {tuple} -- vector 1
        v2 {tuple} -- vector 2
    """

    return (v1[0] + v2[0]) / 2, (v1[1] + v2[1]) / 2
